import React from "react";
import { useHistory } from "react-router-dom";

const TodoCard = (props) => {
  const { todo } = props;
  const { title, completed, id } = todo;
  let history = useHistory();
  return (
    <div
      style={{
        backgroundColor: "CornflowerBlue",
        margin: "10px",
        padding: "15px",
        width: "150px",
        borderRadius: "25px",
      }}
      onClick={() => history.push(`/todos/${id}`)}
    >
      <h4>{title}</h4>
      <h6>{`Completed: ${completed}`}</h6>
    </div>
  );
};

export default TodoCard;
