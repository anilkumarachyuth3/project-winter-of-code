import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { CircularProgress } from "@material-ui/core";
const Todo = () => {
  const { id } = useParams();

  const [todoDetails, setTodoDetails] = useState();

  useEffect(() => {
    axios
      .get(`https://jsonplaceholder.typicode.com/todos/${id}`)
      .then((res) => {
        const responseTodo = res.data;
        setTodoDetails(responseTodo);
      });
  }, [id]);
  const { id: todoId, userId, title, completed } = todoDetails || {};
  return (
    <div
      style={{
        padding: "100px",
        backgroundColor: "Indigo",
        borderRadius: "15px",
      }}
    >
      {todoDetails ? (
        <div>
          <h2>{`Todo ID : ${todoId}`}</h2>
          <h3>{`Todo userID : ${userId}`}</h3>
          <h3>{`Title : ${title}`}</h3>
          <h4>{`Completed : ${completed}`}</h4>
        </div>
      ) : (
        <CircularProgress />
      )}
    </div>
  );
};

export default Todo;
